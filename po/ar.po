# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the flare package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: flare\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-06-16 14:32+0200\n"
"PO-Revision-Date: 2024-08-11 11:09+0000\n"
"Last-Translator: Moa'th <irondragondev@gmail.com>\n"
"Language-Team: Arabic <https://hosted.weblate.org/projects/"
"schmiddi-on-mobile/flare/ar/>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Weblate 5.7-dev\n"

#: data/resources/ui/about.blp:13
msgid "translator-credits"
msgstr "رصيد المترجم"

#: data/resources/ui/channel_info_dialog.blp:50
#: data/resources/ui/setup_window.blp:369
msgid "Phone Number"
msgstr "رقم الهاتف"

#: data/resources/ui/channel_info_dialog.blp:66
msgid "Description"
msgstr "الوصف"

#: data/resources/ui/channel_info_dialog.blp:76
msgid "Disappearing Messages"
msgstr "الرسائل المختفية"

#: data/resources/ui/channel_info_dialog.blp:100
msgctxt "accessibility"
msgid "Reset Session"
msgstr "تصفير الجلسة"

#: data/resources/ui/channel_info_dialog.blp:118
msgid "Reset Session"
msgstr "تصفير الجلسة"

#: data/resources/ui/channel_info_dialog.blp:128
msgctxt "accessibility"
msgid "Clear Messages"
msgstr "مسح الرسائل"

#: data/resources/ui/channel_info_dialog.blp:149
msgid "Clear Messages"
msgstr "مسح الرسائل"

#: data/resources/ui/channel_list.blp:10
#: data/resources/ui/channel_messages.blp:15
msgid "Offline"
msgstr "غير متصل"

#: data/resources/ui/channel_list.blp:20
msgid "Search"
msgstr "البحث"

#: data/resources/ui/channel_list.blp:23 data/resources/ui/window.blp:61
msgctxt "accessibility"
msgid "Search"
msgstr "البحث"

#: data/resources/ui/channel_messages.blp:21
msgid "No Channel Selected"
msgstr "لم يتم تحديد قناة"

#: data/resources/ui/channel_messages.blp:22
msgid "Select a channel"
msgstr "اختر قناة"

#: data/resources/ui/channel_messages.blp:181
msgctxt "accessibility"
msgid "Remove the reply"
msgstr "إزالة الرد"

#: data/resources/ui/channel_messages.blp:184
msgctxt "tooltip"
msgid "Remove reply"
msgstr "إزالة رد"

#: data/resources/ui/channel_messages.blp:204
msgctxt "accessibility"
msgid "Remove an attachment"
msgstr "إزالة مرفق"

#: data/resources/ui/channel_messages.blp:207
msgctxt "tooltip"
msgid "Remove attachment"
msgstr "إزالة مرفق"

#: data/resources/ui/channel_messages.blp:228
msgctxt "accessibility"
msgid "Add an attachment"
msgstr "إضافة مرفق"

#: data/resources/ui/channel_messages.blp:236
msgctxt "tooltip"
msgid "Add attachment"
msgstr "إضافة مرفق"

#: data/resources/ui/channel_messages.blp:252
msgctxt "accessibility"
msgid "Message input"
msgstr "مُدخَل الرسالة"

#: data/resources/ui/channel_messages.blp:258
msgctxt "tooltip"
msgid "Message input"
msgstr "مُدخَل الرسالة"

#: data/resources/ui/channel_messages.blp:270
msgctxt "accessibility"
msgid "Send"
msgstr "إرسال"

#: data/resources/ui/channel_messages.blp:274
msgctxt "tooltip"
msgid "Send"
msgstr "إرسال"

#: data/resources/ui/device_info_item.blp:34
msgid "Linked:"
msgstr "مرتبط:"

#: data/resources/ui/device_info_item.blp:50
msgid "Last Active:"
msgstr "آخر تفاعل:"

#: data/resources/ui/device_info_item.blp:72
#, fuzzy
#| msgid "Linked Devices"
msgid "Unlink Device"
msgstr "الأجهزة المرتبطة"

#: data/resources/ui/dialog_clear_messages.blp:5
#: data/resources/ui/window.blp:166
msgid "Clear messages"
msgstr "مسح الرسائل"

#: data/resources/ui/dialog_clear_messages.blp:6
msgid ""
"This will clear all locally stored messages. This will also close the "
"application."
msgstr ""
"سيمسح هذا جميع الرسائل المخزّنة محليًا. سيقوم هذا الفعل أيضًا بإغلاق التطبيق."

#: data/resources/ui/dialog_clear_messages.blp:11
#: data/resources/ui/dialog_unlink.blp:11
#: data/resources/ui/linked_devices_window.blp:50
#: src/gui/linked_devices_window.rs:122 src/gui/window.rs:309
msgid "Cancel"
msgstr "إلغاء"

#: data/resources/ui/dialog_clear_messages.blp:12
msgid "Clear"
msgstr "امحُ"

#: data/resources/ui/dialog_unlink.blp:5 data/resources/ui/window.blp:155
#: src/gui/linked_devices_window.rs:121
msgid "Unlink"
msgstr "إلغاء الربط"

#: data/resources/ui/dialog_unlink.blp:6
msgid ""
"This will unlink this device and remove all locally saved data. This will "
"also close the application such that it can be relinked when opening it "
"again."
msgstr ""
"سيقوم هذا الفعل بإلغاء ربط هذا الجهاز وحذف جميع البيانات المخزنة محليًا. سيتم "
"أيضًا إغلاق التطبيق والسماح بإعادة الربط في حال فتحه مجددًا."

#: data/resources/ui/dialog_unlink.blp:12
msgid "Unlink and delete all messages"
msgstr "إلغاء الربط وحذف جميع الرسائل"

#: data/resources/ui/dialog_unlink.blp:13
msgid "Unlink but keep messages"
msgstr "إلغاء الربط مع إبقاء الرسائل"

#: data/resources/ui/error_dialog.blp:5
msgid "Error"
msgstr "خطأ"

#: data/resources/ui/error_dialog.blp:26
msgid "Please consider reporting this error."
msgstr "من فضلك، خذ بعين الإعتبار الإبلاغ عن هذا الخطأ."

#: data/resources/ui/error_dialog.blp:43 data/resources/ui/setup_window.blp:478
#: data/resources/ui/submit_captcha_dialog.blp:28
msgid "Close"
msgstr "إغلاق"

#: data/resources/ui/error_dialog.blp:44
msgid "Report"
msgstr "إبلاغ"

#: data/resources/ui/linked_devices_window.blp:13
#: data/resources/ui/window.blp:160
msgid "Linked Devices"
msgstr "الأجهزة المرتبطة"

#: data/resources/ui/linked_devices_window.blp:31
msgid "Add Linked Device"
msgstr "أضف جهازًا مرتبطًا"

#: data/resources/ui/linked_devices_window.blp:32
msgid "Please insert the URL of the device to link."
msgstr "رجاءً قم بإدخال مسار الجهاز للربط."

#: data/resources/ui/linked_devices_window.blp:42
msgid "Device URL"
msgstr "مسار الجهاز"

#: data/resources/ui/linked_devices_window.blp:51
msgid "Add Device"
msgstr "إضافة جهاز"

#: data/resources/ui/message_item.blp:13
msgid "Reply"
msgstr "رد"

#: data/resources/ui/message_item.blp:20
msgid "Delete"
msgstr "حذف"

#: data/resources/ui/message_item.blp:28
msgid "Copy"
msgstr "نسخ"

#: data/resources/ui/message_item.blp:37
msgid "Save as…"
msgstr "حفظ باسم…"

#: data/resources/ui/message_item.blp:45
msgid "Open with…"
msgstr "فتح باستخدام…"

#: data/resources/ui/new_channel_dialog.blp:14
msgid "New Chat"
msgstr "محادثة جديدة"

#: data/resources/ui/new_channel_dialog.blp:20 data/resources/ui/window.blp:49
msgctxt "accessibility"
msgid "Menu"
msgstr "القائمة"

#: data/resources/ui/new_channel_dialog.blp:35
msgid "Search Contacts"
msgstr "بحث عن جهة اتصال"

#: data/resources/ui/new_channel_dialog.blp:71
msgid "List Contacts By:"
msgstr "عرض جهات الاتصال حسب:"

#: data/resources/ui/new_channel_dialog.blp:73
msgid "First Name"
msgstr "الإسم الأول"

#: data/resources/ui/new_channel_dialog.blp:78
msgid "Surname"
msgstr "اللقب"

#: data/resources/ui/preferences_window.blp:6
msgid "General"
msgstr "عام"

#: data/resources/ui/preferences_window.blp:10
msgid "Automatically Download Attachments"
msgstr "تنزيل المرفقات تلقائيًا"

#: data/resources/ui/preferences_window.blp:11
msgid "Attachment types selected below will be automatically downloaded"
msgstr "المرفقات المحددة سيتم تنزيلها تلقائيًا"

#: data/resources/ui/preferences_window.blp:14
msgid "Images"
msgstr "الصور"

#: data/resources/ui/preferences_window.blp:18
msgid "Videos"
msgstr "المقاطع"

#: data/resources/ui/preferences_window.blp:22
msgid "Files"
msgstr "الملفات"

#: data/resources/ui/preferences_window.blp:26
msgid "Voice Messages"
msgstr "الرسائل الصوتية"

#: data/resources/ui/preferences_window.blp:31
msgid "Notifications"
msgstr "الإشعارات"

#: data/resources/ui/preferences_window.blp:32
msgid "Notifications for new messages"
msgstr "الإشعارات للرسائل الجديدة"

#: data/resources/ui/preferences_window.blp:35
msgid "Send Notifications"
msgstr "ارسال الإشعارات"

#: data/resources/ui/preferences_window.blp:39
msgid "Send Notifications on Reactions"
msgstr "ارسال الإشعارات عند التفاعل مع الرسالة"

#: data/resources/ui/preferences_window.blp:44
msgid "Background Notifications"
msgstr "الإشعارات في الخلفية"

#: data/resources/ui/preferences_window.blp:45
msgid "Fetch notifications while the app is closed"
msgstr "طلب الإشعارات والتطبيق مغلق"

#: data/resources/ui/preferences_window.blp:52
msgid "Mobile Compatibility"
msgstr "التوافق مع الجوال"

#: data/resources/ui/preferences_window.blp:53
msgid ""
"These options may want to be changed for a better experience on touch- and "
"mobile devices. The default values of those preferences are chosen to be "
"useful on desktop devices."
msgstr ""
"قد ترغب في تغيير هذه الخيارات لتناسب شاشات اللمس والجوالات، القيمة "
"الإفتراضية لهذه الإعدادات مخصصة لتكون فعالة على الحاسوب."

#: data/resources/ui/preferences_window.blp:56
msgid "Selectable Message Text"
msgstr "رسالة قابلة للتحديد"

#: data/resources/ui/preferences_window.blp:57
msgid "Selectable text can interfere with swipe-gestures on touch-screens"
msgstr "النص القابل للتحديد قد يتعارض من إيماءات اللمس والسحب"

#: data/resources/ui/preferences_window.blp:61
msgid "Press “Enter” to Send Message"
msgstr "اضغط \"Enter\" لإرسال الرسالة"

#: data/resources/ui/preferences_window.blp:62
msgid ""
"It may not be possible to send messages with newlines on touch keyboards not "
"allowing “Control + Enter”"
msgstr ""
"قد لا تتمكن من ارسال رسائل على لوحة المفاتيح اللمس التي لا تدعم إنشاء سطر "
"جديد بإختصار \"Control + Enter\""

#: data/resources/ui/setup_window.blp:17 data/resources/ui/setup_window.blp:24
msgid "Welcome to Flare"
msgstr "أهلًا بك في فلير"

#: data/resources/ui/setup_window.blp:41
msgid "Set Up Flare"
msgstr "تجهيز فلير"

#. Translators: Flare name in metainfo.
#: data/resources/ui/setup_window.blp:50
#: data/de.schmidhuberj.Flare.desktop.in.in:5
#: data/de.schmidhuberj.Flare.metainfo.xml.in:8
msgid "Flare"
msgstr "شرارة"

#: data/resources/ui/setup_window.blp:53
msgid ""
"Flare is an unofficial Signal client. Note that Flare is not stable and "
"there are bugs to be expected. If you are experiencing any bugs, consult the "
"<a href=\"https://gitlab.com/schmiddi-on-mobile/flare/-/issues\">issue "
"tracker</a> and open issues if you are experiencing a new issue. Also note "
"that due to being a third-party application, Flare cannot guarantee the same "
"level of security and privacy as official Signal applications do. If you or "
"someone you need to contact requires a strong level of security, do not use "
"Flare. Consult the <a href=\"https://gitlab.com/schmiddi-on-mobile/flare/-/"
"blob/master/README.md#security\">README</a> for more information."
msgstr ""
"فلير هو عميل غير رسمي لتطبيق Signal. يُرجى ملاحظة أن فلير غير مستقر وقد تواجه "
"بعض الأخطاء. إذا كنت تواجه أي أخطاء، فراجع <a href=\"https://gitlab.com/"
"schmiddi-on-mobile/flare/-/issues\">متتبع المشكلات</a> وقم بفتح مشكلة جديدة "
"إذا كنت تواجه مشكلة جديدة. كما يجب أن تعلم أنه نظرًا لكونه تطبيقًا من طرف ثالث،"
" لا يمكن لفلير ضمان نفس مستوى الأمان والخصوصية الذي توفره تطبيقات Signal "
"الرسمية. إذا كنت أنت أو شخص بحاجة للتواصل معه بحاجة إلى مستوى عالٍ من الأمان، "
"فلا تستخدم فلير. راجع <a href=\"https://gitlab.com/schmiddi-on-mobile/flare/"
"-/blob/master/README.md#security\">ملف README</a> لمزيد من المعلومات."

#: data/resources/ui/setup_window.blp:62 data/resources/ui/setup_window.blp:69
msgid "Setup Primary Device or Link Device?"
msgstr "التعيين كجهاز رئيسي أو ربط الجهاز؟"

#: data/resources/ui/setup_window.blp:85 data/resources/ui/setup_window.blp:317
#: data/resources/ui/setup_window.blp:324
msgid "Primary Device"
msgstr "تعيين كجهاز رئيسي"

#: data/resources/ui/setup_window.blp:98 data/resources/ui/setup_window.blp:153
#: data/resources/ui/setup_window.blp:160
#: data/resources/ui/setup_window.blp:177
msgid "Link Device"
msgstr "ربط الجهاز"

#: data/resources/ui/setup_window.blp:105
msgid "Primary Device or Link Device?"
msgstr "التعيين كجهاز رئيسي أو ربط الجهاز؟"

#: data/resources/ui/setup_window.blp:117
msgid "You can use Flare either as a primary or secondary device."
msgstr "يمكنك استخدام فلير كجهاز رئيسي او ثانوي."

#: data/resources/ui/setup_window.blp:126
msgid "Linked Device (Recommended)"
msgstr "الأجهزة المرتبطة (موصى به)"

#: data/resources/ui/setup_window.blp:131
msgid ""
"Flare can be an application linked to your primary device, similar to the "
"official Signal Desktop. This mode has been more extensively tested and "
"mostly works."
msgstr ""
"يمكن استخدام فلير كتطبيق مرتبط بجهازك الرئيسي، كطريقة عمل تطبيق Signal "
"الرسمي على الحاسوب. هذا الوضع مجرب ويعمل في الغالب."

#: data/resources/ui/setup_window.blp:140
msgid "Primary Device (Disabled)"
msgstr "جهاز رئيسي (معطّل)"

#: data/resources/ui/setup_window.blp:145
msgid ""
"Flare can act as a primary device, similar to the official Signal "
"applications on Android or IOS. This mode is currently not supported by "
"Flare, even though initial tests seem to indicate that it works. If you are "
"willing to test it, you may enable the button below (and if you don't know "
"how to do that, you should not think about testing primary-device support)."
msgstr ""
"يمكن لفلير ان يعمل كجهاز رئيسي، كما في تطبيقات Signal الرسمية على اندرويد و "
"IOS. هذا الوضع غير مدعوم حاليًا من فلير، رغم ان الإختبارات الأولية تظهر انه "
"يعمل. إذا كنت ترغب في تجربته، بإمكانك تفعيله من الزر التالي (في حال لم تكن "
"تعرف طريقة التفعيل، يرجى عدم التفكير بتجربة \"التفعيل كجهاز رئيسي\")."

#: data/resources/ui/setup_window.blp:194
msgid ""
"For linking as a secondary device, Flare will need a device name that will "
"be shown in the official application."
msgstr ""
"للربط كجهاز ثانوي، فلير يحتاج إلى تسمية الجهاز وهذا الإسم سيظهر في التطبيق "
"الرسمي."

#: data/resources/ui/setup_window.blp:204
msgid "Device Name"
msgstr ""

#: data/resources/ui/setup_window.blp:208
#: data/resources/ui/setup_window.blp:377
msgid "Developer Options"
msgstr ""

#: data/resources/ui/setup_window.blp:211
#: data/resources/ui/setup_window.blp:380
msgid "Signal Servers"
msgstr ""

#: data/resources/ui/setup_window.blp:217
msgid ""
"The next page will show a QR code which has to be scanned from the primary "
"device. Note that you only have a timeframe of about one minute to scan the "
"code, so please already prepare your official application for scanning the "
"QR code."
msgstr ""

#: data/resources/ui/setup_window.blp:225
#: data/resources/ui/setup_window.blp:237
#: data/resources/ui/setup_window.blp:291
msgid "Link device"
msgstr ""

#: data/resources/ui/setup_window.blp:242
msgid "Scan Code"
msgstr ""

#: data/resources/ui/setup_window.blp:243
msgid "Scan this code with another Signal app logged into your account."
msgstr ""

#: data/resources/ui/setup_window.blp:270
msgid "Link without scanning"
msgstr ""

#: data/resources/ui/setup_window.blp:280
msgid "Manual linking"
msgstr ""

#: data/resources/ui/setup_window.blp:296
msgid "Manual Linking"
msgstr ""

#: data/resources/ui/setup_window.blp:297
msgid ""
"If your device can't scan the QR code, you can manually enter the link "
"provided here"
msgstr ""

#: data/resources/ui/setup_window.blp:308
msgid "Copy to clipboard"
msgstr ""

#: data/resources/ui/setup_window.blp:341
msgid "Continue"
msgstr ""

#: data/resources/ui/setup_window.blp:358
msgid ""
"For registering as a primary device, Flare will need your phone number and a "
"completion of the captcha. Please give the phone number in international "
"E.164-format, including the leading \"+\" and country code. The captcha can "
"be completed <a href=\"https://signalcaptchas.org/registration/generate."
"html\">here</a>, afterwards copy the \"Open Signal\" link and paste it into "
"the below field. Note that this captcha is only valid for about one minute, "
"so please proceed fast."
msgstr ""

#: data/resources/ui/setup_window.blp:373
#: data/resources/ui/submit_captcha_dialog.blp:20
msgid "Captcha"
msgstr ""

#: data/resources/ui/setup_window.blp:386
msgid ""
"After submitting this information, you will get a SMS from Signal with a "
"verification code. Insert this verification code on the next page."
msgstr ""

#: data/resources/ui/setup_window.blp:395
#: data/resources/ui/setup_window.blp:403
#: data/resources/ui/setup_window.blp:420
#: data/resources/ui/setup_window.blp:445
msgid "Confirm"
msgstr ""

#: data/resources/ui/setup_window.blp:436
msgid "Insert the verification code you received from Signal here."
msgstr ""

#: data/resources/ui/setup_window.blp:453
#: data/resources/ui/setup_window.blp:461
msgid "Finished"
msgstr ""

#: data/resources/ui/setup_window.blp:494
msgid ""
"Flare is almost ready, it is waiting for contacts to finish syncing. This "
"may take a few seconds, you can start to use Flare once the contacts are "
"synced. A few final notes before you start using Flare:"
msgstr ""

#: data/resources/ui/setup_window.blp:503
msgid "Known Bugs"
msgstr ""

#: data/resources/ui/setup_window.blp:508
msgid ""
"Make sure to not send any messages while the messages are still received by "
"Flare, this makes sure that all required information is received. We are "
"trying to fix this problem. Furthermore, sending to contacts you have not "
"yet received messages from can lead to rate limiting. Make sure to receive a "
"message from a contact before you send too many. For a full list of bugs, "
"visit the <a href=\"https://gitlab.com/schmiddi-on-mobile/flare/-/issues/?"
"label_name%5B%5D=bug\">issue tracker</a>."
msgstr ""

#: data/resources/ui/setup_window.blp:518
msgid "Contact"
msgstr ""

#: data/resources/ui/setup_window.blp:523
msgid ""
"Want to leave some feedback about Flare, or just talk a little bit? Our <a "
"href=\"https://matrix.to/#/#flare-signal:matrix.org\">Matrix room</a> is "
"open for everyone."
msgstr ""

#: data/resources/ui/shortcuts.blp:11
msgctxt "shortcut window"
msgid "General"
msgstr ""

#: data/resources/ui/shortcuts.blp:14
msgctxt "shortcut window"
msgid "Show shortcuts"
msgstr ""

#: data/resources/ui/shortcuts.blp:19
msgctxt "shortcut window"
msgid "Go to settings"
msgstr ""

#: data/resources/ui/shortcuts.blp:24
msgctxt "shortcut window"
msgid "Go to about page"
msgstr ""

#: data/resources/ui/shortcuts.blp:29
msgctxt "shortcut window"
msgid "Open menu"
msgstr ""

#: data/resources/ui/shortcuts.blp:34
msgctxt "shortcut window"
msgid "Close window"
msgstr ""

#: data/resources/ui/shortcuts.blp:40
msgctxt "shortcut window"
msgid "Channels"
msgstr ""

#: data/resources/ui/shortcuts.blp:43
msgctxt "shortcut window"
msgid "Go to channel 1…9"
msgstr ""

#: data/resources/ui/shortcuts.blp:48
msgctxt "shortcut window"
msgid "Search in channels"
msgstr ""

#: data/resources/ui/shortcuts.blp:53
msgctxt "shortcut window"
msgid "Upload attachment"
msgstr ""

#: data/resources/ui/shortcuts.blp:58
msgctxt "shortcut window"
msgid "Focus the text input box"
msgstr ""

#: data/resources/ui/shortcuts.blp:63
msgctxt "shortcut window"
msgid "Load more messages"
msgstr ""

#: data/resources/ui/submit_captcha_dialog.blp:5
#: data/resources/ui/window.blp:171
msgid "Submit Captcha"
msgstr ""

#. Translators: Do not translate tags around "on this website".
#: data/resources/ui/submit_captcha_dialog.blp:7
msgid ""
"Submit a Captcha challenge. The token can be obtained from the error message "
"that was displayed from Flare. The captcha must be filled out <a "
"href=\"https://signalcaptchas.org/challenge/generate.html\">on this website</"
"a> and the link to open Signal must be pasted to the corresponding entry. "
"Note that the captcha is only valid for about one minute."
msgstr ""

#: data/resources/ui/submit_captcha_dialog.blp:16
msgid "Token"
msgstr ""

#: data/resources/ui/submit_captcha_dialog.blp:29
msgid "Submit"
msgstr ""

#: data/resources/ui/text_entry.blp:13
msgid "Message"
msgstr ""

#: data/resources/ui/text_entry.blp:40
msgctxt "accessibility"
msgid "Insert an emoji"
msgstr ""

#: data/resources/ui/text_entry.blp:48
msgctxt "tooltip"
msgid "Insert emoji"
msgstr ""

#: data/resources/ui/window.blp:25
msgid "Channel list"
msgstr ""

#: data/resources/ui/window.blp:38
msgctxt "accessibility"
msgid "Add Conversation"
msgstr ""

#: data/resources/ui/window.blp:79
msgid "Chat"
msgstr ""

#: data/resources/ui/window.blp:119 src/gui/channel_item.rs:59
msgid "is typing"
msgstr ""

#: data/resources/ui/window.blp:150
msgid "Settings"
msgstr ""

#: data/resources/ui/window.blp:176
msgid "Synchronize Contacts"
msgstr ""

#: data/resources/ui/window.blp:181
msgid "Keybindings"
msgstr ""

#: data/resources/ui/window.blp:186
msgid "About"
msgstr ""

#: data/resources/ui/window.blp:191
msgid "Quit"
msgstr ""

#: src/backend/channel.rs:537 src/backend/channel.rs:679
msgid "Note to self"
msgstr ""

#: src/backend/contact.rs:193 src/backend/contact.rs:275
msgid "Unknown contact"
msgstr ""

#: src/backend/manager.rs:442
msgid "You"
msgstr ""

#: src/backend/message/call_message.rs:100
msgid "Incoming call"
msgstr ""

#: src/backend/message/call_message.rs:101
msgid "Outgoing call"
msgstr ""

#: src/backend/message/call_message.rs:102
msgid "Call started"
msgstr ""

#: src/backend/message/call_message.rs:103
msgid "Call ended"
msgstr ""

#: src/backend/message/call_message.rs:104
msgid "Call declined"
msgstr ""

#: src/backend/message/call_message.rs:105
msgid "Unanswered call"
msgstr ""

#: src/backend/message/reaction_message.rs:72
msgid "{sender} reacted {emoji} to a message."
msgstr ""

#: src/backend/message/reaction_message.rs:80
msgid "Reacted {emoji} to a message."
msgstr ""

#: src/backend/message/text_message.rs:381
msgid "Sent an image"
msgid_plural "Sent {} images"
msgstr[0] ""
msgstr[1] ""

#: src/backend/message/text_message.rs:383
msgid "Sent an video"
msgid_plural "Sent {} videos"
msgstr[0] ""
msgstr[1] ""

#: src/backend/message/text_message.rs:386
msgid "Sent a voice message"
msgid_plural "Sent {} voice messages"
msgstr[0] ""
msgstr[1] ""

#: src/backend/message/text_message.rs:391
msgid "Sent a file"
msgid_plural "Sent {} files"
msgstr[0] ""
msgstr[1] ""

#: src/error.rs:91
msgid "I/O Error."
msgstr ""

#: src/error.rs:96
msgid "There does not seem to be a connection to the internet available."
msgstr ""

#: src/error.rs:101
msgid "Something glib-related failed."
msgstr ""

#: src/error.rs:106
msgid "The communication with Libsecret failed."
msgstr ""

#: src/error.rs:111
msgid ""
"The backend database failed. Please restart the application or delete the "
"database and relink the application."
msgstr ""

#: src/error.rs:116
msgid ""
"You do not seem to be authorized with Signal. Please delete the database and "
"relink the application."
msgstr ""

#: src/error.rs:121
msgid "Sending a message failed."
msgstr ""

#: src/error.rs:126
msgid "Receiving a message failed."
msgstr ""

#: src/error.rs:131
msgid ""
"Something unexpected happened with the signal backend. Please retry later."
msgstr ""

#: src/error.rs:136
msgid "The application seems to be misconfigured."
msgstr ""

#: src/error.rs:141
msgid "A part of the application crashed."
msgstr ""

#: src/error.rs:151
msgid "Please check your internet connection."
msgstr ""

#: src/error.rs:156
msgid "Please delete the database and relink the device."
msgstr ""

#: src/error.rs:163
msgid "The database path at {} is no folder."
msgstr ""

#: src/error.rs:168
msgid "Please restart the application with logging and report this issue."
msgstr ""

#: src/gui/channel_info_dialog.rs:197
msgid "Never"
msgstr ""

#: src/gui/channel_item.rs:63
msgid "Draft"
msgstr ""

#: src/gui/linked_devices_window.rs:116
#, fuzzy
#| msgid "Linked Devices"
msgid "Unlink Device?"
msgstr "الأجهزة المرتبطة"

#: src/gui/linked_devices_window.rs:117
msgid "Are you sure you want to unlink {}?"
msgstr ""

#: src/gui/preferences_window.rs:25
msgid "Watch for new messages while closed"
msgstr ""

#: src/gui/preferences_window.rs:99
msgid "Background permission"
msgstr ""

#: src/gui/preferences_window.rs:100
msgid "Use settings to remove permissions"
msgstr ""

#: src/gui/setup_window.rs:34
msgctxt "Signal Server"
msgid "Production"
msgstr ""

#: src/gui/setup_window.rs:38
msgctxt "Signal Server"
msgid "Staging"
msgstr ""

#: src/gui/setup_window.rs:132
msgid "Copied to clipboard"
msgstr ""

#. How to format time. Should probably be %H:%M (meaning print hours from 00-23, then a :,
#. then minutes from 00-59). For a full list of supported identifiers, see <https://docs.gtk.org/glib/method.DateTime.format.html>
#: src/gui/utility.rs:102 src/gui/utility.rs:126
msgid "%H:%M"
msgstr ""

#. How to format a date with time. Should probably be similar to %Y-%m-%d %H:%M (meaning print year, month from 01-12, day from 01-31 (each separated by -), hours from 00-23, then a :,
#. then minutes from 00-59). For a full list of supported identifiers, see <https://docs.gtk.org/glib/method.DateTime.format.html>
#: src/gui/utility.rs:105
msgid "%Y-%m-%d %H:%M"
msgstr ""

#: src/gui/utility.rs:138
msgid "Today"
msgstr ""

#: src/gui/utility.rs:141
msgid "Yesterday"
msgstr ""

#. How to format a human-readable date including the year. Should probably be similar to %Y-%m-%d (meaning print year, month from 01-12, day from 01-31 (each separated by -)).
#. For a full list of supported identifiers, see <https://docs.gtk.org/glib/method.DateTime.format.html>
#: src/gui/utility.rs:147
msgid "%Y-%m-%d"
msgstr ""

#. How to format a human-readable date excluding the year. Should probably be similar to "%a., %d. %b" (meaning print abbreviated weekday, day from 1-31 and abbreviated month name).
#. For a full list of supported identifiers, see <https://docs.gtk.org/glib/method.DateTime.format.html>
#: src/gui/utility.rs:151
msgid "%a., %e. %b"
msgstr ""

#: src/gui/utility.rs:159
msgid "Attachment"
msgstr ""

#: src/gui/window.rs:304 src/gui/window.rs:310
msgid "Remove Messages"
msgstr ""

#: src/gui/window.rs:305
msgid "This will remove all locally stored messages from this channel"
msgstr ""

#. Translators: Flare summary in metainfo.
#: data/de.schmidhuberj.Flare.desktop.in.in:6
#: data/de.schmidhuberj.Flare.metainfo.xml.in:10
msgid "Chat with your friends on Signal"
msgstr ""

#: data/de.schmidhuberj.Flare.desktop.in.in:15
msgid "messaging;chat;signal;"
msgstr ""

#. Translators: Description of Flare in metainfo.
#: data/de.schmidhuberj.Flare.metainfo.xml.in:29
msgid ""
"Flare is an unofficial app for Signal. It is still in development and "
"doesn't include all the features that the official Signal apps do. More "
"information can be found on its feature roadmap."
msgstr ""

#. Translators: Description of Flare in metainfo: Security note
#: data/de.schmidhuberj.Flare.metainfo.xml.in:33
msgid ""
"Please note that using this application will probably worsen your security "
"compared to using official Signal applications. Use with care when handling "
"sensitive data. Look at the projects README for more information about "
"security."
msgstr ""

#: data/de.schmidhuberj.Flare.metainfo.xml.in:609
msgid "Overview of the application"
msgstr ""

#: data/de.schmidhuberj.Flare.gschema.xml:6
msgid "Window width"
msgstr ""

#: data/de.schmidhuberj.Flare.gschema.xml:10
msgid "Window height"
msgstr ""

#: data/de.schmidhuberj.Flare.gschema.xml:14
msgid "Window maximized state"
msgstr ""

#: data/de.schmidhuberj.Flare.gschema.xml:19
msgid ""
"The device name when linking Flare. This requires the application to be re-"
"linked."
msgstr ""

#: data/de.schmidhuberj.Flare.gschema.xml:24
msgid "Automatically download images"
msgstr ""

#: data/de.schmidhuberj.Flare.gschema.xml:28
msgid "Automatically download videos"
msgstr ""

#: data/de.schmidhuberj.Flare.gschema.xml:32
msgid "Automatically download voice messages"
msgstr ""

#: data/de.schmidhuberj.Flare.gschema.xml:36
msgid "Automatically download files"
msgstr ""

#: data/de.schmidhuberj.Flare.gschema.xml:41
msgid "Send notifications"
msgstr ""

#: data/de.schmidhuberj.Flare.gschema.xml:45
msgid "Run in background when closed"
msgstr ""

#: data/de.schmidhuberj.Flare.gschema.xml:49
msgid "Send notifications when retrieving reactions"
msgstr ""

#: data/de.schmidhuberj.Flare.gschema.xml:54
msgid "Can messages be selected"
msgstr ""

#: data/de.schmidhuberj.Flare.gschema.xml:58
msgid "Send a message when the Enter-key is pressed"
msgstr ""

#: data/de.schmidhuberj.Flare.gschema.xml:63
msgid "How to sort contacts, e.g with \"firstname\" or \"surname\""
msgstr ""
